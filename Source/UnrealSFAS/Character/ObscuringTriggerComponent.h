// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealSFAS/Util/TriggerComponent.h"
#include "ObscuringTriggerComponent.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSFAS_API UObscuringTriggerComponent : public UTriggerComponent
{
	GENERATED_BODY()

	protected:
	virtual void TriggerCallbackOn(UPrimitiveComponent* OtherComp) override;
	virtual void TriggerCallbackOff(UPrimitiveComponent* OtherComp) override;

	virtual bool IsValid(AActor* Actor, UPrimitiveComponent* Comp) override;
};
