// Fill out your copyright notice in the Description page of Project Settings.

#include "ObscuringTriggerComponent.h"
#include "UnrealSFAS/Obscurable/Obscurable.h"

void UObscuringTriggerComponent::TriggerCallbackOn(UPrimitiveComponent* OtherComp)
{
	IObscurable* Obscurable;
	if (IObscurable* OActor = Cast<IObscurable>(OtherComp->GetOwner()))
		Obscurable = OActor;
	else if (IObscurable* OComp = Cast<IObscurable>(OtherComp))
		Obscurable = OComp;
	else
		return;
	
	Obscurable->Obscure();
}

void UObscuringTriggerComponent::TriggerCallbackOff(UPrimitiveComponent* OtherComp)
{
	IObscurable* Obscurable;
	if (IObscurable* OActor = Cast<IObscurable>(OtherComp->GetOwner()))
		Obscurable = OActor;
	else if (IObscurable* OComp = Cast<IObscurable>(OtherComp))
		Obscurable = OComp;
	else
		return;
	
	Obscurable->UnObscure();
}

bool UObscuringTriggerComponent::IsValid(AActor* Actor, UPrimitiveComponent* Comp)
{
	return Cast<IObscurable>(Actor) || Cast<IObscurable>(Comp);
}

