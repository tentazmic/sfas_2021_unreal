// Fill out your copyright notice in the Description page of Project Settings.


#include "AttakerChaser.h"

// Sets default values
AAttakerChaser::AAttakerChaser()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AAttakerChaser::Obscure()
{
	IObscurable::Obscure();
	SetActorTickEnabled(false);
	GetMesh()->SetVisibility(false);

	BPObscure();
}

void AAttakerChaser::UnObscure()
{
	IObscurable::UnObscure();
	SetActorTickEnabled(true);
	GetMesh()->SetVisibility(true);

	BPUnObscure();
}
