// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UnrealSFAS/Obscurable/Obscurable.h"

#include "AttakerChaser.generated.h"

UCLASS()
class UNREALSFAS_API AAttakerChaser : public ACharacter, public IObscurable
{
	GENERATED_BODY()

public:
	AAttakerChaser();

	virtual void Obscure() override;
	virtual void UnObscure() override;

	UFUNCTION(BlueprintImplementableEvent)
	void BPObscure();
	UFUNCTION(BlueprintImplementableEvent)
	void BPUnObscure();
};
