// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSFASMaze.h"
#include "UnrealSFAS/Environment/MazeWallComponent.h"

// Sets default values
AUnrealSFASMaze::AUnrealSFASMaze()
{
 	// No need to tick the maze at this point
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AUnrealSFASMaze::BeginPlay()
{
	Super::BeginPlay();

	if (WallMesh)
	{
		const int32 MazeSize = 20;
		const float BlockSize = 200.0f;
		const float BlockWidth = 2.0f;
		const float BlockHeight = 5.0f;
		const float BlockZPos = 50.0f;

		// Array of binary values: 1 = wall, 0 = space
		uint32 MazeArray[MazeSize] = {	0b11111111111111111111,
										0b10100000000000000001,
										0b10101010111111111101,
										0b10101010001111000101,
										0b10101010100001010001,
										0b10101010111111111111,
										0b10101010000000000001,
										0b10001011111111111101,
										0b11111000000000000101,
										0b10001111111111111101,
										0b10101111111111111001,
										0b10100000000000001101,
										0b10111011111111101001,
										0b10100010000000101011,
										0b10101110111110101001,
										0b10100000100010101101,
										0b10101110001110100001,
										0b10111111111110111101,
										0b10000000000000000001,
										0b11111111111111111101 };

		float XPos = 0.0f;
		float YPos = 0.0f;
		const FQuat WorldRotation(FVector(0.0f, 0.0f, 1.0f), 0.0f);
		const FVector WorldScale(BlockWidth, BlockWidth, BlockHeight);

		// USceneComponent* RootComponent = GetRootComponent();

		// Loop through the binary values to generate the maze as static mesh components attached to the root of this actor
		for (int32 i = 0; i < MazeSize; i++)
		{
			YPos = static_cast<float>(i - (MazeSize / 2)) * BlockSize;
			const uint32 MazeRow = MazeArray[i];

			for (int32 j = 0; j < MazeSize; j++)
			{
				XPos = static_cast<float>(j - (MazeSize / 2)) * BlockSize;

				const uint32 MazeValue = (MazeRow >> (MazeSize - (j + 1))) & 1;

				if (MazeValue)
				{
					UStaticMeshComponent* MeshComponent = NewObject<UMazeWallComponent>(this);
					FVector WorldPosition(XPos, YPos, BlockZPos);
					FTransform WorldXForm(WorldRotation, WorldPosition, WorldScale);

					MeshComponent->SetStaticMesh(WallMesh);
					MeshComponent->SetWorldTransform(WorldXForm);
					MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
					MeshComponent->RegisterComponent();
				}
			}
		}

	}
}
