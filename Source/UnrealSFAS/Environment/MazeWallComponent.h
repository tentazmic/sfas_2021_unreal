﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealSFAS/Obscurable/Obscurable.h"
#include "MazeWallComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNREALSFAS_API UMazeWallComponent : public UStaticMeshComponent, public IObscurable
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void Obscure() override;

	UFUNCTION()
	virtual void UnObscure() override;
};
