﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeWallComponent.h"

void UMazeWallComponent::Obscure()
{
	IObscurable::Obscure();
	SetActive(false);
	SetVisibility(false);
}

void UMazeWallComponent::UnObscure()
{
	IObscurable::UnObscure();
	SetActive(true);
	SetVisibility(true);
}

