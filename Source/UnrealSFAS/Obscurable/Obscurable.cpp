﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Obscurable.h"


// Add default functionality here for any IObscurable functions that are not pure virtual.
void IObscurable::Obscure()
{
	bIsObscured = true;
}

void IObscurable::UnObscure()
{
	bIsObscured = false;
}

