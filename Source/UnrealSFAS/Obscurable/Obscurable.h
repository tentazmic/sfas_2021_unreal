﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Obscurable.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UObscurable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class UNREALSFAS_API IObscurable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	bool bIsObscured;

	virtual void Obscure();
	virtual void UnObscure();
};
