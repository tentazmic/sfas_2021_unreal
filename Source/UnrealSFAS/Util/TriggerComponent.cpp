// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerComponent.h"

#include "Components/BoxComponent.h"

UTriggerComponent::UTriggerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	// SetupShapeComponent();
	UBoxComponent* BoxTrigger = CreateDefaultSubobject<UBoxComponent>(FName("Trigger Shape"));
	BoxTrigger->SetupAttachment(reinterpret_cast<USceneComponent*>(this));
	BoxTrigger->SetBoxExtent({ 200, 1000, 50 });
	BoxTrigger->SetGenerateOverlapEvents(true);
	Shape = BoxTrigger;
}

// void UTriggerComponent::SetupShapeComponent()
// {
// 	UBoxComponent* BoxTrigger = CreateDefaultSubobject<UBoxComponent>(FName("Trigger Shape"));
// 	BoxTrigger->SetupAttachment(reinterpret_cast<USceneComponent*>(this));
// 	BoxTrigger->SetBoxExtent({ 200, 1000, 50 });
// 	BoxTrigger->SetGenerateOverlapEvents(true);
// 	Shape = BoxTrigger;
// }

void UTriggerComponent::BeginPlay()
{
	Super::BeginPlay();
	BindTriggerCallbacks();
}

void UTriggerComponent::BindTriggerCallbacks()
{
	if (!Shape) return;

	Shape->OnComponentBeginOverlap
		.RemoveDynamic(this, &UTriggerComponent::OnTriggerOverlapBegin);
	Shape->OnComponentEndOverlap
		.RemoveDynamic(this, &UTriggerComponent::OnTriggerOverlapEnd);

	Shape->OnComponentBeginOverlap
		.AddDynamic(this, &UTriggerComponent::OnTriggerOverlapBegin);
	Shape->OnComponentEndOverlap
		.AddDynamic(this, &UTriggerComponent::OnTriggerOverlapEnd);
}

void UTriggerComponent::OnTriggerOverlapBegin(
	UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	// Prevents self collisions and checks if other wants to collide
	if (OtherActor && OtherActor != GetOwner() && IsValid(OtherActor, OtherComp) && (TargetActor == nullptr || TargetActor == OtherActor))
	{
		OverlappingComponents.Add(OtherComp);
		
		TriggerOverlapBeginEvent.Broadcast();
		TriggerCallbackOn(OtherComp);
	}
}

void UTriggerComponent::OnTriggerOverlapEnd(
	UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && OtherActor != GetOwner() && (TargetActor == nullptr || TargetActor == OtherActor))
	{
		if (!OverlappingComponents.Contains(OtherComp))
			return;

		OverlappingComponents.Remove(OtherComp);
		
		TriggerOverlapEndEvent.Broadcast();
		TriggerCallbackOff(OtherComp);
	}
}

void UTriggerComponent::TriggerCallbackOn(UPrimitiveComponent* OtherComp)
{
	UE_LOG(LogTemp, Warning, TEXT("UTriggerComponent::TriggerCallbackOn"));
}

void UTriggerComponent::TriggerCallbackOff(UPrimitiveComponent* OtherComp)
{
	UE_LOG(LogTemp, Warning, TEXT("UTriggerComponent::TriggerCallbackOff"));
}

void UTriggerComponent::SetBoxLocation(FVector NewLocation) const
{
	Shape->SetWorldLocation(NewLocation);
}

