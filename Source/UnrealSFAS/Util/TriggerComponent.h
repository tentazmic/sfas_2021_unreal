// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "TriggerComponent.generated.h"


class UBoxComponent;
UCLASS( ClassGroup=(Custom), Abstract, meta=(BlueprintSpawnableComponent) )
class UNREALSFAS_API UTriggerComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UTriggerComponent();
	
	DECLARE_EVENT(UTriggerComponen, FTriggerComponentEvent)
    FTriggerComponentEvent TriggerOverlapBeginEvent;
	FTriggerComponentEvent TriggerOverlapEndEvent;

protected:
	/** 
	 * A pointer to which Actor this trigger should react to
	 * If null, it will react to all
	 */
	UPROPERTY(EditAnywhere)
	AActor* TargetActor = nullptr;
	
	UPROPERTY(VisibleAnyWhere, Category=Setup)
	UBoxComponent* Shape = nullptr;

	TArray<UPrimitiveComponent*> OverlappingComponents;
	
	// UFUNCTION()
	// void SetupShapeComponent();

	/** Delegate for Shape's overlap begin event */
	UFUNCTION()
	void OnTriggerOverlapBegin(
		UPrimitiveComponent* OverlappedComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult
		);

	/** Delegate for Shape's overlap end event */
	UFUNCTION()
	void OnTriggerOverlapEnd(
		UPrimitiveComponent* OverlappedComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex
		);

	/** Adds callbacks to the Shape component's begin and end overlap events */
	UFUNCTION()
	void BindTriggerCallbacks();

	/** Is run when OnComponentBeginOverlap() is called. Override to add functionality */
	UFUNCTION()
	virtual void TriggerCallbackOn(UPrimitiveComponent* OtherComp);

	/** Is run when OnComponentEndOverlap() is called. Override to add functionality */
	UFUNCTION()
	virtual void TriggerCallbackOff(UPrimitiveComponent* OtherComp);

	/* Use to check any addition requirements of the objects */
	UFUNCTION()
	virtual bool IsValid(AActor* Actor, UPrimitiveComponent* Comp) { return true; }

	virtual void BeginPlay() override;

public:
	TArray<UPrimitiveComponent*> const* GetAllOverlappingComponents() const { return &OverlappingComponents; }

	void SetBoxLocation(FVector NewLocation) const;
};
