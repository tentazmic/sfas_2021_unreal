# Project Plan

## Introduction

The Unreal project came as a small third person game where the player has to traverse a runtime generated maze and reach an exit.
The player camera is held at the end of an arm, which is moved around via the mouse; the arm retracts when there is a wall blocking the path
between the arm's origin and the camera.

This'll be my first time working in Unreal (although I do have some experience in C++), so I'm not looking to do anything too complex.  .
I've never liked it when games force the camera to zoom in when something gets in the way. It's incredibly claustrophobic, and nine times out of ten simply turning of the renderer for the terrain is a much better solution. I think that idea will be a solid foundation for the greater concept.

## Initial Concept

Build up on the initial idea of obscuring parts of the level, and think a great development would be a puzzle/stealth game.  
I'll have unkillable enemies chase down the player, the only way that can be stopped is by getting them inbetween the player, and the camera.
They'll become invisible, or have a marker on them, which will be frozen until they are no longer in the obscurable zone. This will also go for
any blocks in the way.

Certain things will not be obscurable, these can be special objects that benefit from having things move out of the way. For example, if you obscure a block, a physics object sitting on top of it will fall down.
